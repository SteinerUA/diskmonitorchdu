CHDU Disk Monitor Service
=======================
>Маленький Web-сервис для мониторинга свободного места на жестких дисках в компьютерных классах.


Использование репозитория
=======================
*Для работы с репозиторием необходимо использовать среду разработки `PhpStorm` версии 8.0.1 или выше,
    систему контроля версий `Git`, систему автоматической сборки проекта `GruntJS` и менеджер пакетов `Bower`*.

  1. Загружаем и устанавливаем необходимые для работы инструменты: [PhpStorm][1], [Git][2]. 
    Об установке [GruntJS][3], [Bower][4] будет написано дальше. 
    
  2. Во время установки `Git` (для Windows) на этапе `Adjusting your PATH environment` (4 окно)
    выбрать пункт `Run Git from the Windows Command Prompt`. 
    Это необходимо для того, чтобы можно было выполнять `git <command>` из консоли.
    
  3. Для использования `GruntJS` и `Bower` вам понадобится установить [Node.js][5] (на Mac просто brew install node). 
    Сложностей с установкой `Node.js` возникнуть не должно.    

  4. После установки `Node.js` из консоли выполнить такие команды: 
    - `npm install -g bower`
    - `npm install -g grunt-cli`

  5. Запускаем `PhpStorm`. В стартовом окне выбора проектов `PhpStorm` выбираем пункт `Check out from Versoin Control` и
    подпункт `Git`, в появившемся окне вводим адрес репозитория, выбираем локальный путь по которому загружать
    репозиторий и нажимаем на кнопку `Ok`. В случае появления запроса на авторизацию - вводим свой логин и пароль
    от репозитория.     
    
  5. После успешного копирования проекта и открытия его в `PhpStorm`. Из консоли, находясь в папке с проектом выполнить:
    - `npm install`
    - `bower install`

  6. *(Optional)* Радуемся жизни.


Минимальные системные требования
=======================
  1. PHP >= 5.3.


Разработчики
=======================
- Ihor Kushnirenko - <SteinerOk@gmail.com>
- [Yevhen Matiiuk](https://bitbucket.org/maje16)
- [Serg Stavitckii](https://bitbucket.org/sergstav)


Использованные библиотеки и фреймворки
-----------------------
 * [Flight](http://flightphp.com/)
 * [bootstrap](http://getbootstrap.com/)
 * [jquery](http://jquery.com/)
 * [chartjs](http://www.chartjs.org/)
 * [html5shiv](https://github.com/aFarkas/html5shiv)
 * [respond](https://github.com/scottjehl/Respond)
 * [vegas](http://vegas.jaysalvat.com/)


Лицензия
=======================
	Copyright 2014 Ihor Kushnirenko, Yevhen Matiiuk, Serg Stavitckii
	
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.


 [1]: https://www.jetbrains.com/phpstorm/download/
 [2]: http://git-scm.com/
 [3]: http://gruntjs.com/
 [4]: http://bower.io/
 [5]: http://nodejs.org/download/