<?php
/**
 * Project: CHDU-DiskMonitor
 * Created by: Ihor Kushnirenko <SteinerOk@gmail.com>
 * Date: 21.10.2014 1:36
 */

session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Вход - CHDU Disk Monitor</title>
    <!-- Fonts -->
    <!--<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400italic,700italic,400,700" rel="stylesheet"
          type="text/css">-->
    <!-- build:css ../assets/css/styles.bower.min.css -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <!-- endbuild -->
    <!-- build:css ../assets/css/styles.min.css -->
    <link href="../assets/css/styles.css" rel="stylesheet">
    <!-- endbuild -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!-- build:js ../assets/js/scripts.bowerIEfix.min.js -->
    <script src="../bower_components/html5shiv/dist/html5shiv.min.js"></script>
    <script src="../bower_components/respond/dest/respond.min.js"></script>
    <!-- endbuild -->
    <![endif]-->
</head>
<body>
<!-- Форма авторизации -->
<div class="container signin-container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <h1 class="text-center login-title">Вход в систему CHDU Disk Monitor</h1>
            <?php if ($loginError == 'authenticate'): ?>
                <div class="alert alert-danger fade in" role="alert">
                    <h4>Упс! Ошибка аутентификации!</h4>

                    <p>Невозможно выполнить аутентификацию. Возможно Вы допустили ошибку в логине или пароле.</p>
                </div>
            <?php elseif ($loginError == 'authorize'): ?>
                <div class="alert alert-danger fade in" role="alert">
                    <h4>Упс! Ошибка авторизации!</h4>

                    <p>Невозможно выполнить авторизацию. Возможно у Вас нет прав для входа.</p>
                </div>
            <?php endif; ?>
            <div class="account-wall">
                <img class="profile-img" src="../assets/img/photo.png" alt="">

                <form class="form-signin" id="loginForm" method="POST" action="./login" accept-charset="UTF-8">
                    <input id="login" name="login" type="text" class="form-control" placeholder="Логин" required
                           autofocus>
                    <input id="password" name="password" type="password" class="form-control" placeholder="Пароль"
                           required>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Войти</button>
                    <label class="checkbox pull-left">
                        <input name="rememberMe" type="checkbox" checked value="remember-me">
                        Запомнить
                    </label>
                    <!--<a href="#" data-toggle="modal" data-target="#helpModal" class="pull-right need-help">
                        Нужна помощь? </a>--><span class="clearfix"></span>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Подсказка -->
<!--TODO: Сделать подсказку-->
<!--<div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="helpModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span>
                    <span class="sr-only">Закрыть</span></button>
                <h4 class="modal-title" id="myModalLabel">Помощь</h4>
            </div>
            <div class="modal-body">
                Текст справочной информации
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Спасибо!</button>
            </div>
        </div>
    </div>
</div>-->
<!-- build:js ../assets/js/scripts.bower.min.js -->
<script src="../bower_components/jquery/dist/jquery.js"></script>
<script src="../bower_components/bootstrap/dist/js/bootstrap.js"></script>
<!-- endbuild -->
</body>
</html>
