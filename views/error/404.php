<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>404 - Page not found</title>
    <!--<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400' rel='stylesheet' type='text/css'>-->
    <!-- build:css ../../assets/css/styles.bower.min.css -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <link href="../../bower_components/vegas/dist/jquery.vegas.css" rel="stylesheet">
    <!-- endbuild -->
    <!-- build:css ../../assets/css/errorPages.min.css -->
    <link href="../../assets/css/errorPages.css" rel="stylesheet">
    <!-- endbuild -->
    <!--[if lt IE 9]>
    <!-- build:js ../../assets/js/scripts.bowerIEfix.min.js -->
    <script src="../../bower_components/html5shiv/dist/html5shiv.min.js"></script>
    <script src="../../bower_components/respond/dest/respond.min.js"></script>
    <!-- endbuild -->
    <![endif]-->
</head>
<body>

<div class="container">
    <div class="row" id="text">
        <div class="col-md-12">
            <h1>Упс, 404</h1>

            <div class="line"><h3>Page not found</h3></div>
            <p>К сожалению, запрашиваемая Вами страница не была найдена.<br>
                Выбор у Вас не велик:( Вы можете вернутся на один шаг назад нажав
                <span class="highlight">Вернутся назад</span>,<br>
                или перейти на главную страницу нажав <span class="highlight">Вернутся домой</span>.<br>
                Удачи!
            </p>
            <a class="btn btn-primary" onclick="window.history.back(-1);">Вернутся назад</a>
            &emsp;&emsp;&emsp;
            <a class="btn btn-primary" href="/">Вернутся домой</a>
        </div>
    </div>
</div>

<!-- build:js ../../assets/js/scripts.bower.min.js -->
<script src="../../bower_components/jquery/dist/jquery.js"></script>
<script src="../../bower_components/bootstrap/dist/js/bootstrap.js"></script>
<!-- endbuild -->
<!-- build:js ../../assets/js/scripts.bower.vegas.min.js -->
<script src="../../bower_components/vegas/dist/jquery.vegas.js"></script>
<!-- endbuild -->
<script>
    $(document).ready(function () {
        $.vegas({
            src: '../../assets/img/bg-404.jpg'
        });
        $.vegas('overlay', {
            src: '../../assets/img/bg-error-overlay.png'
        });
    });
</script>
</body>
</html>