<?php
/**
 * Project: CHDU-DiskMonitor
 * Created by: Ihor Kushnirenko <SteinerOk@gmail.com>
 * Date: 01.11.2014 0:42
 */

session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo strtoupper($classNumber); ?> - CHDU Disk Monitor</title>
    <!-- Fonts -->
    <!--<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400italic,700italic,400,700" rel="stylesheet"
          type="text/css">-->
    <!-- build:css ../assets/css/styles.bower.min.css -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <!-- endbuild -->
    <!-- build:css ../assets/css/styles.min.css -->
    <link href="../assets/css/styles.css" rel="stylesheet">
    <!-- endbuild -->
    <!-- build:js ../assets/js/scripts.bower.min.js -->
    <script src="../bower_components/jquery/dist/jquery.js"></script>
    <script src="../bower_components/bootstrap/dist/js/bootstrap.js"></script>
    <!-- endbuild -->
    <!-- build:js ../assets/js/scripts.bower.Chart.min.js -->
    <script src="../bower_components/chartjs/Chart.js"></script>
    <!-- endbuild -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!-- build:js ../assets/js/scripts.bowerIEfix.min.js -->
    <script src="../bower_components/html5shiv/dist/html5shiv.min.js"></script>
    <script src="../bower_components/respond/dest/respond.min.js"></script>
    <!-- endbuild -->
    <![endif]-->
</head>
<body>
<!-- Navigation ================================================== -->
<header class="navbar navbar-static-top navbar-fixed-top navbar-default" id="top" role="banner">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">CHDU Disk Monitor</a>
        </div>

        <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/logout">Выйти</a></li>
            </ul>
        </nav>
    </div>
</header>
<!-- Main content ================================================== -->
<main class="container-fluid dashboard-container" id="content" role="main">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <script>
                $(document).ready(function () {
                    $("#" + "<?php echo $classNumber; ?>").addClass("active");
                });
            </script>
            <ul class="nav nav-sidebar">
                <?php
                foreach ($classLocations as $class => $location) {
                    echo '<li id="' . $class . '"><a href="/dashboard/' . $class . '">
                    ' . strtoupper($class) . ' (' . $location . ')</a></li>';
                }
                unset($class);
                unset($location);
                ?>
            </ul>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main-content">
            <?php include 'inc/dashboard.module.php'; ?>
        </div>
    </div>
</main>
<!-- Footer ================================================== -->
<footer class="bs-docs-footer" role="contentinfo">
    <div class="container col-sm-offset-3 col-md-offset-2">
        <p>CHDU Disk Monitor Service. Project licensed under the
            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache License v2.0</a></p>
        <ul class="bs-docs-footer-links muted">
            <li>Текущая версия – v<?php echo Flight::get('version'); ?></li>
            <li>&middot;</li>
            <li><a href="https://bitbucket.org/SteinerUA/chdu-diskmonitor">BitBucket</a></li>
            <li>&middot;</li>
            <li><a href="https://bitbucket.org/SteinerUA/chdu-diskmonitor/wiki/Home">Wiki</a></li>
            <li>&middot;</li>
            <li>
                <a href="https://bitbucket.org/SteinerUA/chdu-diskmonitor/issues?status=new&status=open">Issues</a>
            </li>
        </ul>
    </div>
</footer>
</body>
</html>
