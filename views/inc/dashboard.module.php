<?php
/**
 * Project: CHDU-DiskMonitor
 * Created by: Ihor Kushnirenko <SteinerOk@gmail.com>
 * Date: 01.11.2014 16:37
 *
 * Доступные переменные:
 * $classNumber - выбранный класс, например, "l-9"
 * $computers - массив имен компьютеров в выбраном классе, в формате, "s-01"
 * $adminLogin - логин пользователя, например, "kma\ihor"
 * $adminPassword - пароль пользователя, открытый вид
 * $classLocations - асоциативный массив адресов компьютерных классов (не используется)
 *
 * $noSidebar - "true", если пользователь админ в одном классе, иначе не определено
 *
 * $allowedClasses - массив имен классов, доступных пользователю (не используется)
*/

if (count($computers) > 0) {
    ?>
    <h1 class="page-header">Дисковое пространство в <?php echo strtoupper($classNumber); ?> </h1>
    <!--Основные данные-->
    <div class="row placeholders">
        <?
        $drivesCount = 0;
        foreach ($computers as $computer) {
            $output = null;
            if (Flight::get('develop')) {
                $command = "wmic /node:\"$computer\" logicaldisk where drivetype=3 get Name,Size,FreeSpace,VolumeName /format:csv";
            } else {
                $computer = $computer . ".kma.edu";
                $command = "wmic /node:\"$computer\" /user:\"$adminLogin\" /password:\"$adminPassword\" logicaldisk where drivetype=3 get Name,Size,FreeSpace,VolumeName /format:csv";
            }
            exec($command, $output);
            ?>
            <div class="col-md-<?php echo ($noSidebar == true) ? "2" : "3"; ?> placeholder">
                <div class="panel panel-default text-center">
                    <div class="panel-heading"><?php echo $computer; ?></div>
                    <?php
                    foreach ($output as $drive) {
                        if ($drive == "" || $drive == "Node,FreeSpace,Name,Size,VolumeName") {
                        } else {
                            $data = explode(",", $drive);
                            $totalDrive = $data[3];
                            if ($totalDrive != 0) {
                                $totalDriveGB = round($data[3] / 1073741824);
                                $freeDrive = $data[1];
                                $freeDriveGB = round($data[1] / 1073741824);
                                $freePercent = round($freeDrive / $totalDrive * 100);
                                $usedDrive = $totalDrive - $freeDrive;
                                $usedDriveGB = round($usedDrive / 1073741824);
                                $usedPercent = round($usedDrive / $totalDrive * 100);
                                ?>
                                <div class="panel-body">
                                    <div class="canvas-holder">
                                        <canvas id="disk-chart-<? echo ++$drivesCount; ?>"></canvas>
                                    </div>

                                    <script>
                                        Chart.defaults.global.responsive = true;
                                        (function () {
                                            var data = [
                                                {
                                                    value: <? echo $usedDriveGB?>,
                                                    color: "#F7464A",
                                                    highlight: "#FF5A5E",
                                                    label: "Занято"
                                                },
                                                {
                                                    value: <? echo $freeDriveGB?>,
                                                    color: "#46BFBD",
                                                    highlight: "#5AD3D1",
                                                    label: "Свободно"
                                                }
                                            ];
                                            var options = {
                                                segmentShowStroke: true,
                                                animateScale: false,
                                                tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %> Гб"
                                            };
                                            var $id = function (id) {
                                                return document.getElementById(id);
                                            };
                                            var canvas = $id('disk-chart-<? echo $drivesCount?>');
                                            var diskChart = new Chart(canvas.getContext('2d')).Doughnut(data, options);
                                        })();
                                    </script>

                                    <?php
                                    echo '<h4>Диск ' . $data[2] . '(' . $totalDriveGB . ' Гб)</h4>
                                <span class="text-muted">' . $freeDriveGB . ' Гб свободно (' . $freePercent . '%)</span>
                                <br>
                                <span class="text-muted">' . $usedDriveGB . ' Гб занято (' . $usedPercent . '%)</span>';
                                    ?>
                                </div>
                            <?php
                            } else {
                                echo '<div class="panel-body "> Не удалось получить информацию с данного ПК </div>';
                            }
                        }
                    }
                    ?>
                </div>
            </div>
        <?php
        }
        ?>
    </div>
<?php } else { ?>
    <div class="alert alert-danger fade in" role="alert">
        <strong>Ошибка!</strong> Отсутствует список компьютеров для выбраного класса или файл с адресами
        компьютеров недоступен.
    </div>
<?php } ?>