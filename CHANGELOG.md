## 1.0.1 (2014-11-11)
#### Available viewing space on the hard drive for all classes (exclude L13)

## 1.0.0 (2014-11-08)
#### Available viewing space on the hard drive, but only for L2: D

## 0.1.1 (2014-11-04)
#### Fix bug with LDAP. And yes, the main function (viewing space on the hard drive) - missing: D

## 0.1.0 (2014-11-04)
#### The first release:) And yes, the main function (viewing space on the hard drive) - missing: D