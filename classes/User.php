<?php

/**
 * Project: CHDU-DiskMonitor
 * Created by: Ihor Kushnirenko <SteinerOk@gmail.com>
 * Date: 23.10.2014 20:51
 */

error_reporting(0);

class User
{
    public $login;
    public $password;
    public $groups;

    public function __construct($login, $password)
    {
        $this->login = $login;
        $this->password = $password;
        $this->groups = array();
    }

    public function tryAuthenticate($ldapDomain, $ldapHost, $ldapPort)
    {
        //TODO: Протестировать!
        if (Flight::get('develop')) {
            return true;
        } else {
            //Подсоединяемся к LDAP серверу
            $ldapConnect = ldap_connect($ldapHost, $ldapPort) or die("Cant connect to LDAP Server");
            //Включаем LDAP протокол версии 3
            ldap_set_option($ldapConnect, LDAP_OPT_PROTOCOL_VERSION, 3) or die("Failed to set LDAP protocol to version 3");
            if ($ldapConnect) {
                return ldap_bind($ldapConnect, $this->login . $ldapDomain, $this->password);
            } else {
                return false;
            }
        }
    }

    public function tryAuthorizeAsAdmin()
    {
        $output = null;
        if (Flight::get('develop')) {
            $command = "net user " . $this->login;
            exec($command, $output);
            $output_str = implode("<br>", $output);
            $output_str = iconv("cp866", "UTF-8", $output_str);
            $pattern = '/(.*Членство в локальных группах)(.*)(Членство в глобальных группах.*)/i'; //локальные группы, русский язык.
        } else {
            $command = "net user /domain " . $this->login;
            exec($command, $output);
            $output_str = implode("<br>", $output);
            $output_str = iconv("cp866", "UTF-8", $output_str);
            $pattern = '/(.*Global Group memberships)(.*)(The command completed successfully.*)/i'; // List of Global Group memberships
        }
        $replacement = '${2}';
        $output_str = preg_replace($pattern, $replacement, $output_str);
        global $adminGroups;
        foreach ($adminGroups as $key => $value) {
            if (preg_match('/\*' . $key . '/', $output_str)) {
                array_push($this->groups, $value);
            }
        }
        return (count($this->groups) > 0);
    }

}