// Стандартный экспорт модуля в NodeJS
module.exports = function (grunt) {
    'use strict';
    // 1. Всё конфигурирование тут
    grunt.initConfig({
        // 2. Загрузка файла "package.json" с описанием проекта
        pkg: grunt.file.readJSON('package.json'),
        // 3. Свойства для упрощения конфигурирования
        properties: {
            releaseDir: 'production',
            releaseViewsDir: '<%= properties.releaseDir %>/views',
            releaseAssetsDir: '<%= properties.releaseDir %>/assets',
            releaseCssDir: '<%= properties.releaseAssetsDir %>/css',
            releaseJsDir: '<%= properties.releaseAssetsDir %>/js',
            releaseTemp: ['<%= properties.releaseJsDir %>/scripts.js', '.tmp'],
            releaseArchiveDir: 'productionArchive'
        },
        // Настройки модулей GruntJS, их нужно предварительно установить через менеджер пакетов npm, или
        // добавить в файл package.json в раздел "devDependencies" перед запуском npm install
        // 4.1. Конфигурация для удаления папок и файлов
        clean: {
            preRelease: ['<%= properties.releaseDir %>/*'],
            postRelease: ['<%= properties.releaseTemp %>']
        },
        // 4.2. Конфигурация автопрефиксов (moz, webkit) для css файлов
        autoprefixer: {
            options: {
                browsers: ['> 0.4%', 'Opera >=12', 'Firefox >= 15', 'Chrome >= 25', 'Explorer >= 8']
            },
            release: {
                expand: true,
                flatten: true,
                src: 'assets/css/*.css',
                dest: 'assets/css/'
            }
        },
        // 4.3. Конфигурация причесывания css файлов
        csscomb: {
            options: {
                sortOrder: '.csscomb.json'
            },
            release: {
                expand: true,
                cwd: 'assets/css/',
                src: '*.css',
                dest: 'assets/css/',
                ext: '.css'
            }
        },
        // 4.4. Конфигурация для копирования
        copy: {
            release: {
                files: [
                    {
                        expand: true,
                        src: ['assets/fonts/**', 'assets/img/**'],
                        dest: '<%= properties.releaseDir %>'
                    },
                    {
                        expand: true,
                        src: ['assets/js/*.min.js', 'assets/css/*.min.css'],
                        dest: '<%= properties.releaseDir %>'
                    },
                    {
                        expand: true,
                        src: ['.htaccess', '*.php', 'views/**', 'technical/**', 'classes/**', 'flight/**'],
                        dest: '<%= properties.releaseDir %>'
                    }
                ]
            },
            bower: {
                files: [
                    {
                        expand: true,
                        cwd: 'bower_components/bootstrap/dist',
                        src: ['/fonts/**'],
                        dest: '<%= properties.releaseAssetsDir %>'
                    }
                ]
            }
        },
        // 4.5. Конфигурация для объединения JS файлов
        concat: {
            release: {
                src: ['assets/js/*.js', '!assets/js/*.min.js'],
                dest: '<%= properties.releaseJsDir %>/scripts.js'
            },
            bower: {
                src: [
                    'bower_components/jquery/dist/jquery.min.js',
                    'bower_components/bootstrap/dist/js/bootstrap.min.js'
                ],
                dest: '<%= properties.releaseJsDir %>/scripts.bower.min.js'
            },
            bowerChartJS: {
                src: [
                    'bower_components/chartjs/Chart.min.js'
                ],
                dest: '<%= properties.releaseJsDir %>/scripts.bower.Chart.min.js'
            },
            bowerVegas: {
                src: [
                    'bower_components/vegas/dist/jquery.vegas.min.js'
                ],
                dest: '<%= properties.releaseJsDir %>/scripts.bower.vegas.min.js'
            },
            bowerIEfix: {
                src: [
                    'bower_components/html5shiv/dist/html5shiv.min.js',
                    'bower_components/respond/dest/respond.min.js'
                ],
                dest: '<%= properties.releaseJsDir %>/scripts.bowerIEfix.min.js'
            }
        },
        // 4.6. Конфигурация для сжатия js файлов
        uglify: {
            release: {
                options: {
                    banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */',
                    report: 'gzip'
                },
                files: {
                    '<%= properties.releaseJsDir %>/scripts.min.js': '<%= concat.release.dest %>'
                }
            }
        },
        // 4.7. Конфигурация для минификации и конкатенации css файлов
        cssmin: {
            release: {
                options: {
                    banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */',
                    report: 'gzip'
                },
                files: {
                    '<%= properties.releaseCssDir %>/styles.min.css': ['assets/css/*.css', '!assets/css/*.min.css', '!assets/css/errorPages.css'],
                    '<%= properties.releaseCssDir %>/errorPages.min.css': ['assets/css/errorPages.css']
                }
            },
            bower: {
                files: {
                    '<%= properties.releaseCssDir %>/styles.bower.min.css': [
                        'bower_components/bootstrap/dist/css/bootstrap.min.css',
                        'bower_components/vegas/dist/jquery.vegas.min.css'
                    ]
                }
            }
        },
        // 4.8. Конфигурация для заменны ссылок на js и css
        useminPrepare: {
            html: ['<%= properties.releaseViewsDir %>/**/*.php'],
            options: {
                dest: '<%= properties.releaseViewsDir %>'
            }
        },
        usemin: {
            html: ['<%= properties.releaseViewsDir %>/**/*.php']
        },
        // 4.9. Конфигурация для получения данных из Git
        gitinfo: {
            commands: {
                'lastCommitTime': ['log', '--date=short', '--pretty=format:%cd', '-n1', 'HEAD'],
                'lastTag': ['describe', '--tags', '--dirty', '--long', '--abbrev=10']
            }
        },
        // 4.10. Конфигурация для архивации содежимого production в папку productionArchive
        zip: {
            'release': {
                cwd: '<%= properties.releaseDir %>',
                src: ['<%= properties.releaseDir %>/**'],
                dest: '<%= properties.releaseArchiveDir %>/<%= pkg.name %>_v.<%= pkg.version  %>_' +
                'build.<%= gitinfo.local.branch.current.lastCommitNumber %>_<%= gitinfo.lastCommitTime %>_' +
                '<%= gitinfo.local.branch.current.name %>_<%= gitinfo.local.branch.current.shortSHA %>.zip',
                dot: true
            }
        }
    });

    // 5. Задаем список используемых плагинов для Grunt
    grunt.loadNpmTasks('grunt-contrib-clean'); // плагин для удаления папок и файлов
    grunt.loadNpmTasks('grunt-autoprefixer'); //автопрефиксы (moz, webkit) к css свойствам
    grunt.loadNpmTasks('grunt-csscomb'); // плагин для "причесывания" css файла
    grunt.loadNpmTasks('grunt-contrib-copy'); // плагин для копирования папок и файлов
    grunt.loadNpmTasks('grunt-contrib-concat'); // плагин конкатенации js
    grunt.loadNpmTasks('grunt-contrib-uglify'); // плагин минификации js
    grunt.loadNpmTasks('grunt-contrib-cssmin'); // плагин конкатенации и минификации css
    grunt.loadNpmTasks('grunt-usemin'); // плагин для замены ссылок в html
    grunt.loadNpmTasks('grunt-gitinfo'); // плагин для получении информации из Git
    grunt.loadNpmTasks('grunt-zip'); // плагин для архивации содежимого production в папку productionArchive

    // 6. Составные задачи для Grunt, задача "default" выполнится при вводе "grunt" в терминале из папки проекта
    grunt.registerTask('cssPrepare', ['autoprefixer', 'csscomb']);
    grunt.registerTask('makeRelease', ['clean:preRelease', 'copy', 'concat', 'uglify', 'cssmin', 'usemin', 'clean:postRelease']);
    grunt.registerTask('makeArchive', ['gitinfo', 'zip']);
    grunt.registerTask('default', ['cssPrepare', 'makeRelease', 'makeArchive']);
};