<?php
/**
 * Project: CHDU-DiskMonitor
 * Created by: Ihor Kushnirenko <SteinerOk@gmail.com>
 * Date: 08.10.2014 12:51
 */

session_start();
require 'flight/Flight.php';
require 'technical/config.global.php';
require 'classes/User.php';
require 'classes/IniUtils.php';
require 'classes/CryptUtils.php';

Flight::set('flight.log_errors', true);
Flight::set('develop', $developProject);
Flight::set('version', $versionProject);
Flight::set('ldapDomain', $domain);
Flight::set('ldapHost', $host);
Flight::set('ldapPort', $port);

function tryLoginWithCookie($login, $encryptedPassword)
{
    $user = new User($login, CryptUtils::dsCrypt($encryptedPassword, true));
    if ($user->tryAuthenticate(Flight::get('ldapDomain'), Flight::get('ldapHost'), Flight::get('ldapPort'))) {
        if ($user->tryAuthorizeAsAdmin()) {
            $_SESSION['uid'] = $user->login;
            $_SESSION['upw'] = $encryptedPassword;
            $_SESSION['ugr'] = $user->groups;
            global $adminGroups, $domainAdminGroup;
            if (($user->groups[0] == $adminGroups[$domainAdminGroup]) || (count($user->groups) > 1)) {
                Flight::redirect('/dashboard');
            } elseif (count($user->groups) == 1) {
                Flight::redirect('/dashboard/' . $user->groups[0]);
            }
        } else {
            logout();
        }
    } else {
        logout();
    }
}

function logout()
{
    setcookie('login', '', 0, "/");
    setcookie('password', '', 0, "/");
    session_unset(); // Очистить все переменные сесиии
    session_destroy(); // Удалить всю информацию о сессии
    Flight::redirect('/login');
}

Flight::route('GET /', function () {
    global $adminGroups, $domainAdminGroup;
    if (!isset($_SESSION['uid']) || !isset($_SESSION['upw']) || !isset($_SESSION['ugr'])) {
        if (isset($_COOKIE['login']) && isset($_COOKIE['password'])) {
            tryLoginWithCookie($_COOKIE['login'], $_COOKIE['password']);
        } else {
            Flight::redirect('/login');
        }
    } elseif (($_SESSION['ugr'][0] == $adminGroups[$domainAdminGroup]) || (count($_SESSION['ugr']) > 1)) {
        Flight::redirect('/dashboard');
    } elseif (count($_SESSION['ugr']) == 1) {
        Flight::redirect('/dashboard/' . $_SESSION['ugr'][0]);
    }
});

Flight::route('GET /login', function () {
    global $adminGroups, $domainAdminGroup;
    if (!isset($_SESSION['uid']) || !isset($_SESSION['upw']) || !isset($_SESSION['ugr'])) {
        if (isset($_COOKIE['login']) && isset($_COOKIE['password'])) {
            tryLoginWithCookie($_COOKIE['login'], $_COOKIE['password']);
        } else {
            Flight::view()->set('loginError', $_GET['error']);
            Flight::render('login.php');
        }
    } elseif (($_SESSION['ugr'][0] == $adminGroups[$domainAdminGroup]) || (count($_SESSION['ugr']) > 1)) {
        Flight::redirect('/dashboard');
    } elseif (count($_SESSION['ugr']) == 1) {
        Flight::redirect('/dashboard/' . $_SESSION['ugr'][0]);
    }
});

Flight::route('POST /login', function () {
    if (isset($_POST['login'])) {
        $user = new User($_POST['login'], $_POST['password']);
        if ($user->tryAuthenticate(Flight::get('ldapDomain'), Flight::get('ldapHost'), Flight::get('ldapPort'))) {
            if ($user->tryAuthorizeAsAdmin()) {
                $encryptedPassword = CryptUtils::dsCrypt($user->password);
                $_SESSION['uid'] = $user->login;
                $_SESSION['upw'] = $encryptedPassword;
                $_SESSION['ugr'] = $user->groups;
                if (isset($_POST['rememberMe'])) {
                    $time = 604800; // на 7 дней
                    setcookie('login', $_POST['login'], time() + $time, "/");
                    setcookie('password', $encryptedPassword, time() + $time, "/");
                }
                global $adminGroups, $domainAdminGroup;
                if (($user->groups[0] == $adminGroups[$domainAdminGroup]) || (count($user->groups) > 1)) {
                    Flight::redirect('/dashboard');
                } elseif (count($user->groups) == 1) {
                    Flight::redirect('/dashboard/' . $user->groups[0]);
                }
            } else {
                Flight::redirect('/login?error=authorize');
            }
        } else {
            Flight::redirect('/login?error=authenticate');
        }
    } else {
        Flight::redirect('/login');
    }
});

Flight::route('GET /dashboard', function () {
    global $adminGroups, $domainAdminGroup, $classLocations;
    if (!isset($_SESSION['uid']) || !isset($_SESSION['upw']) || !isset($_SESSION['ugr'])) {
        if (isset($_COOKIE['login']) && isset($_COOKIE['password'])) {
            tryLoginWithCookie($_COOKIE['login'], $_COOKIE['password']);
        } else {
            Flight::redirect('/login');
        }
    } elseif ($_SESSION['ugr'][0] == $adminGroups[$domainAdminGroup]) {
        Flight::view()->set('classLocations', $classLocations);
        Flight::render('index-domain.php');
    } elseif (count($_SESSION['ugr']) == 1) {
        Flight::redirect('/dashboard/' . $_SESSION['ugr'][0]);
    } else {
        Flight::view()->set('allowedClasses', $_SESSION['ugr']);
        Flight::view()->set('classLocations', $classLocations);
        Flight::render('index-local.php');
    }
});

Flight::route('GET /dashboard/@class:l-[1-9]|l-1[1-3]|gl|l-40|l-50', function ($class) { //For test: "|l-40|l-50" - удалить в релизе!
    if (isset($_SESSION['uid']) && isset($_SESSION['upw']) && isset($_SESSION['ugr'])) {
        global $adminGroups, $domainAdminGroup, $classLocations;
        $ini = new IniWorker('technical/config.computers.ini');
        Flight::view()->set('classNumber', $class);
        Flight::view()->set('computers', $ini->getComputers($class));
        Flight::view()->set('adminLogin', "kma\\" . $_SESSION['uid']);
        Flight::view()->set('adminPassword', CryptUtils::dsCrypt($_SESSION['upw'], true));
        Flight::view()->set('classLocations', $classLocations);

        if ($_SESSION['ugr'][0] == $adminGroups[$domainAdminGroup]) {
            Flight::render('dashboard-domain.php');
        } elseif (count($_SESSION['ugr']) > 1) {
            if (in_array($class, $_SESSION['ugr'])) {
                Flight::view()->set('allowedClasses', $_SESSION['ugr']);
                Flight::render('dashboard-local-many.php');
            } else {
                Flight::redirect('/dashboard/local');
            }
        } elseif (in_array($class, $_SESSION['ugr'])) {
            Flight::view()->set('noSidebar', true);
            Flight::render('dashboard-local-one.php');
        } else {
            Flight::redirect('/dashboard/' . $_SESSION['ugr'][0]);
        }

    } elseif (isset($_COOKIE['login']) && isset($_COOKIE['password'])) {
        tryLoginWithCookie($_COOKIE['login'], $_COOKIE['password']);
    } else {
        Flight::redirect('/login');
    }
});

Flight::route('GET /logout', function () {
    logout();
});

Flight::map('notFound', function () {
    Flight::render('error/404.php');
});

Flight::map('error', function () {
    Flight::render('error/500.php');
});

Flight::start();